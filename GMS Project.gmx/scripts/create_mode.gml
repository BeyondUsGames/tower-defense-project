var selectUp = mouse_wheel_up();
var selectDown = mouse_wheel_down();
var selectionChange = selectDown - selectUp;
if selectionChange != 0 {
    var maxSelect = array_length_1d(placeableObjects) - 1;
    var newSelection = selectedObject + selectionChange;
    
    if newSelection > maxSelect
        selectedObject = 0;
    else if newSelection < 0
        selectedObject = maxSelect;
    else
        selectedObject = newSelection;
}

// Set current object according to selection    
obj = placeableObjects[selectedObject];

///Move the object around
x = round(mouse_x/CELL) * CELL + sprite_get_xoffset(obj);
y = round(mouse_y/CELL) * CELL + sprite_get_yoffset(obj);


if mouse_check_button_pressed(mb_left)
&& !place_meeting(mouse_x,mouse_y,obj)
&& !place_meeting(x, y, oEnemy)
&& !place_meeting(x, y, objButtonParent)
{
    newBlock = instance_create(x, y, obj);
    //Add the block to the grid
    mp_grid_add_instances(global.grid, obj, false);
    //Check to see if it blocks the path entirely
    tempPath = path_add();
    if(mp_grid_path(global.grid, tempPath, oStart.x,oStart.y, oFinish.x,oFinish.y,1) == false) {
        instance_destroy(newBlock);
    }
     mp_grid_clear_all(global.grid);  
     mp_grid_add_instances(global.grid, obj, false);
}

///Really inefficient method, but works.
//Should be changed to only run when entering a new cell
else {
    newBlock = instance_create(x, y, obj);
    //Add the block to the grid
    mp_grid_add_instances(global.grid, obj, false);
    //Check to see if it blocks the path entirely
    tempPath = path_add();
    if(mp_grid_path(global.grid, tempPath, oStart.x,oStart.y, oFinish.x,oFinish.y,1) == false) {
        instance_destroy(newBlock);
        legalPlacement = false;
    }
    else {
        instance_destroy(newBlock);
        legalPlacement = true;
    }
     mp_grid_clear_all(global.grid);  
     mp_grid_add_instances(global.grid, obj, false);
}
